jQuery(document).ready(function($) {
	// Toggle Main Nav
	$(".toggle-mnu").click(function() {
		$(".wrapper-main-nav").toggle("slow");
		$(this).toggleClass("active");
		return false;
	});
	// Active testimonial
	$(".testimonial-item").hover(function () {
		$(".testimonial-item").removeClass("active");
		$(this).addClass("active");
	});
	// Active news-item
	$(".news-item").hover(function () {
		$(".news-item").removeClass("active");
		$(this).addClass("active");
	});
	//Open all content post
	$(".open-all-content").click(function() {
		$(this).closest('.type-post').toggleClass('active');
		return false;
	});
	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/auto/mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Спасибо! Сообщение отправлено");
			$.fancybox.close(); //Закрыть pop-up
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
});
